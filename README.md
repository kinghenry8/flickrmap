# mapbox.js

This project was created using the flickr API in conjunction with the mapBox API and getJSON/AJAX jQuery requests. See '217Project.docx' for a more thorough explanation. This demo is running on a node.JS server using command "nohup node app.js &", keeping it running as a background process so it should work unless the server has been rebooted since I ran this process. This demo should work on all browsers including Internet Explorer, Opera, Safari, Chrome, and Firefox.

Implementation: http://invengion.org:5155/


[![Build Status](https://travis-ci.org/mapbox/mapbox.js.png?branch=v1)](https://travis-ci.org/mapbox/mapbox.js)

(work in progress)

All code references to layers should ref `examples.map-20v6611k`
